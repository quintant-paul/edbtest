# Interview Task

# Task
XXX Platform Engineer DevOps Assignment\
The assignment is to provision a WordPress instance on a freshly setup AWS Ubuntu VM.
Together with WordPress, please include the necessary monitoring services and tweaks that you will
want to have should you be supporting and responsible for the production usage of this WordPress
instance.\
Please include steps that was taken to setup the WordPress instance and the necessary monitoring
services. If you can provide documentation or scripts or infra-as-code.

# Manual (provision a WordPress instance on a freshly setup AWS Ubuntu VM)

## Task - Install 
-wordpress \
-php \
-libapache2-mod-php \
-mysql-server \
-php-mysql

## Install wordpress and pre-requisite
```bashcat /
sudo apt update
sudo apt install wordpress php libapache2-mod-php mysql-server php-mysql
```
Result
```
devops-test@ip-172-31-40-210:~$ mysql -V
mysql  Ver 8.0.23-0ubuntu0.20.04.1 for Linux on x86_64 ((Ubuntu))

devops-test@ip-172-31-40-210:~$ apache2 -v
Server version: Apache/2.4.41 (Ubuntu)
Server built:   2020-08-12T19:46:17

devops-test@ip-172-31-40-210:~$  php -version
PHP 7.4.3 (cli) (built: Oct  6 2020 15:47:56) ( NTS )
Copyright (c) The PHP Group
Zend Engine v3.4.0, Copyright (c) Zend Technologies
    with Zend OPcache v7.4.3, Copyright (c), by Zend Technologies

root@ip-172-31-40-210:/usr/share/wordpress/wp-includes# cat version.php|grep -i wp_version
$wp_version = '5.3.2';
```
## Configure wordpress
create wordpress database and user in mysql
```bash
mysql -u root -p 

CREATE USER 'wordpress'@'localhost' IDENTIFIED BY 'password';

GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,DROP,ALTER ON wordpress.* TO 'wordpress'@'localhost';


mysql> select user from user
    -> ;
+------------------+
| user             |
+------------------+
| root             |
| debian-sys-maint |
| mysql.infoschema |
| mysql.session    |
| mysql.sys        |
| root             |
| wordpress        |
+------------------+
7 rows in set (0.00 sec)

mysql> show grants for wordpress@localhost;
+-------------------------------------------------------------------------------------------------------+
| Grants for wordpress@localhost                                                                        |
+-------------------------------------------------------------------------------------------------------+
| GRANT USAGE ON *.* TO `wordpress`@`localhost`                                                         |
| GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, ALTER ON `wordpress`.* TO `wordpress`@`localhost` |
+-------------------------------------------------------------------------------------------------------+
2 rows in set (0.00 sec)
```

set up wp config
```bash
root@ip-172-31-40-210:/usr/share/wordpress/wp-includes# cat  /etc/wordpress/config-localhost.php
<?php
define('DB_NAME', 'wordpress');
define('DB_USER', 'wordpress');
define('DB_PASSWORD', 'password');
define('DB_HOST', 'localhost');
define('DB_COLLATE', 'utf8_general_ci');
define('WP_CONTENT_DIR', '/usr/share/wordpress/wp-content');
?>

```


## Review 
review if wp is up and running
```bash
root@ip-172-31-40-210:/home/devops-test# curl http://localhost/blog/wp-admin/install.php
return a proper html body. 
```
unable to access 80 

workaround set up my instance(http://13.212.71.146/) and open 80 in security group for 0.0.0.0/0

[setup](http://13.212.71.146/blog/wp-admin/install.php)

[test page](http://13.212.71.146/blog/index.php/2021/02/06/test/)


# IaC (provision a WordPress instance on a freshly setup AWS Ubuntu VM)
## Option 1 - Install using Ansible 
## Setup ansible  
from a bastion/jump host
```bash
$ sudo apt update
$ sudo apt install software-properties-common
$ sudo apt-add-repository --yes --update ppa:ansible/ansible
$ sudo apt install ansible
```
enable key-less login from ansible host
```
oot@ip-172-31-45-14:/home/ubuntu# ssh-copy-id -i ~/.ssh/aws ubuntu@13.212.71.146
/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/root/.ssh/aws.pub"
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed

/usr/bin/ssh-copy-id: WARNING: All keys were skipped because they already exist on the remote system.
                (if you think this is a mistake, you may want to use -f option)

```
```bash
root@ip-172-31-45-14:/home/ubuntu# ssh -i ~/.ssh/aws ubuntu@13.212.71.146
Welcome to Ubuntu 20.04.1 LTS (GNU/Linux 5.4.0-1029-aws x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Sat Feb  6 15:55:04 UTC 2021

  System load:  0.0               Processes:             125
  Usage of /:   26.9% of 7.69GB   Users logged in:       1
  Memory usage: 63%               IPv4 address for eth0: 172.31.37.42
  Swap usage:   0%


94 updates can be installed immediately.
40 of these updates are security updates.
To see these additional updates run: apt list --upgradable


Last login: Sat Feb  6 15:41:14 2021 from 121.7.7.19
```
from bastion server
```bash
ubuntu@ip-172-31-45-14:~$ cat /etc/hosts|grep test
13.212.71.146   test

ubuntu@ip-172-31-45-14:~$cat /etc/ansible/hosts
[testgroup]
ansible_ssh_user=ubuntu
test    ansible_ssh_private_key_file=~/.ssh/aws

ubuntu@ip-172-31-45-14:~$ ansible test -m ping
test | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    },
    "changed": false,
    "ping": "pong"
}

```

playbook script for installation  
```
ubuntu@ip-172-31-45-14:~/ansible-playbooks/ansible-playbooks/wordpress-lamp_ubuntu1804$ cat vars/default.yml
---
#System Settings
php_modules: [ 'php-curl', 'php-gd', 'php-mbstring', 'php-xml', 'php-xmlrpc', 'php-soap', 'php-intl', 'php-zip' ]

#MySQL Settings
mysql_root_password: "password"
mysql_db: "wordpress"
mysql_user: "wordpress"
mysql_password: "password"

#HTTP Settings
http_host: "your_domain"
http_conf: "your_domain.conf"
http_port: "80"

ubuntu@ip-172-31-45-14:~/ansible-playbooks/ansible-playbooks/wordpress-lamp_ubuntu1804$ cat  playbook.yml
#########################################################
# DO Community Playbooks: Wordpress on Ubuntu 18.04 LAMP
#########################################################
---
- hosts: all
  become: true
  vars_files:
    - vars/default.yml

  tasks:
    - name: Install prerequisites
      apt: name=aptitude update_cache=yes state=latest force_apt_get=yes
      tags: [ system ]

    - name: Install LAMP Packages
      apt: name={{ item }} update_cache=yes state=latest
      loop: [ 'apache2', 'mysql-server', 'python3-pymysql', 'php', 'php-mysql', 'libapache2-mod-php' ]
      tags: [ system ]

    - name: Install PHP Extensions
      apt: name={{ item }} update_cache=yes state=latest
      loop: "{{ php_modules }}"
      tags: [ system ]

  # Apache Configuration
    - name: Create document root
      file:
        path: "/var/www/{{ http_host }}"
        state: directory
        owner: "www-data"
        group: "www-data"
        mode: '0755'
      tags: [ apache ]

    - name: Set up Apache VirtualHost
      template:
        src: "files/apache.conf.j2"
        dest: "/etc/apache2/sites-available/{{ http_conf }}"
      notify: Reload Apache
      tags: [ apache ]

    - name: Enable rewrite module
      shell: /usr/sbin/a2enmod rewrite
      notify: Reload Apache
      tags: [ apache ]

    - name: Enable new site
      shell: /usr/sbin/a2ensite {{ http_conf }}
      notify: Reload Apache
      tags: [ apache ]

    - name: Disable default Apache site
      shell: /usr/sbin/a2dissite 000-default.conf
      notify: Restart Apache
      tags: [ apache ]

  # MySQL Configuration
    - name: Set the root password
      mysql_user:
        name: root
        password: "{{ mysql_root_password }}"
        login_unix_socket: /var/run/mysqld/mysqld.sock
      tags: [ mysql, mysql-root ]

    - name: Remove all anonymous user accounts
      mysql_user:
        name: ''
        host_all: yes
        state: absent
        login_user: root
        login_password: "{{ mysql_root_password }}"
      tags: [ mysql ]

    - name: Remove the MySQL test database
      mysql_db:
        name: test
        state: absent
        login_user: root
        login_password: "{{ mysql_root_password }}"
      tags: [ mysql ]

    - name: Creates database for WordPress
      mysql_db:
        name: "{{ mysql_db }}"
        state: present
        login_user: root
        login_password: "{{ mysql_root_password }}"
      tags: [ mysql ]

    - name: Create MySQL user for WordPress
      mysql_user:
        name: "{{ mysql_user }}"
        password: "{{ mysql_password }}"
        priv: "{{ mysql_db }}.*:ALL"
        state: present
        login_user: root
        login_password: "{{ mysql_root_password }}"
      tags: [ mysql ]

  # UFW Configuration
    - name: "UFW - Allow HTTP on port {{ http_port }}"
      ufw:
        rule: allow
        port: "{{ http_port }}"
        proto: tcp
      tags: [ system ]

  # WordPress Configuration
    - name: Download and unpack latest WordPress
      unarchive:
        src: https://wordpress.org/latest.tar.gz
        dest: "/var/www/{{ http_host }}"
        remote_src: yes
        creates: "/var/www/{{ http_host }}/wordpress"
      tags: [ wordpress ]

    - name: Set ownership
      file:
        path: "/var/www/{{ http_host }}"
        state: directory
        recurse: yes
        owner: www-data
        group: www-data
      tags: [ wordpress ]

    - name: Set permissions for directories
      shell: "/usr/bin/find /var/www/{{ http_host }}/wordpress/ -type d -exec chmod 750 {} \\;"
      tags: [ wordpress ]

    - name: Set permissions for files
      shell: "/usr/bin/find /var/www/{{ http_host }}/wordpress/ -type f -exec chmod 640 {} \\;"
      tags: [ wordpress ]

    - name: Set up wp-config
      template:
        src: "files/wp-config.php.j2"
        dest: "/var/www/{{ http_host }}/wordpress/wp-config.php"
      tags: [ wordpress ]

  handlers:
    - name: Reload Apache
      service:
        name: apache2
        state: reloaded

    - name: Restart Apache
      service:
        name: apache2
        state: restarted
```

## result 
```bash
ubuntu@ip-172-31-45-14:~/ansible-playbooks/ansible-playbooks/wordpress-lamp_ubuntu1804$ ansible-playbook playbook.yml -l test3 -u ubuntu

PLAY [all] ***************************************************************************************************************************************************************************************************

TASK [Gathering Facts] ***************************************************************************************************************************************************************************************
ok: [test3]

TASK [Install prerequisites] *********************************************************************************************************************************************************************************
changed: [test3]

TASK [Install LAMP Packages] *********************************************************************************************************************************************************************************
changed: [test3] => (item=apache2)
changed: [test3] => (item=mysql-server)
changed: [test3] => (item=python3-pymysql)
changed: [test3] => (item=php)
changed: [test3] => (item=php-mysql)
changed: [test3] => (item=libapache2-mod-php)

TASK [Install PHP Extensions] ********************************************************************************************************************************************************************************
changed: [test3] => (item=php-curl)
changed: [test3] => (item=php-gd)
changed: [test3] => (item=php-mbstring)
changed: [test3] => (item=php-xml)
changed: [test3] => (item=php-xmlrpc)
changed: [test3] => (item=php-soap)
changed: [test3] => (item=php-intl)
changed: [test3] => (item=php-zip)

TASK [Create document root] **********************************************************************************************************************************************************************************
changed: [test3]

TASK [Set up Apache VirtualHost] *****************************************************************************************************************************************************************************
changed: [test3]

TASK [Enable rewrite module] *********************************************************************************************************************************************************************************
changed: [test3]

TASK [Enable new site] ***************************************************************************************************************************************************************************************
changed: [test3]

TASK [Disable default Apache site] ***************************************************************************************************************************************************************************
changed: [test3]

TASK [Set the root password] *********************************************************************************************************************************************************************************
[WARNING]: Module did not set no_log for update_********
changed: [test3]

TASK [Remove all anonymous user accounts] ********************************************************************************************************************************************************************
ok: [test3]

TASK [Remove the MySQL test database] ************************************************************************************************************************************************************************
ok: [test3]

TASK [Creates database for WordPress] ************************************************************************************************************************************************************************
changed: [test3]

TASK [Create MySQL user for WordPress] ***********************************************************************************************************************************************************************
changed: [test3]

TASK [UFW - Allow HTTP on port 80] ***************************************************************************************************************************************************************************
changed: [test3]

TASK [Download and unpack latest WordPress] ******************************************************************************************************************************************************************
changed: [test3]

TASK [Set ownership] *****************************************************************************************************************************************************************************************
changed: [test3]

TASK [Set permissions for directories] ***********************************************************************************************************************************************************************
changed: [test3]

TASK [Set permissions for files] *****************************************************************************************************************************************************************************
changed: [test3]

TASK [Set up wp-config] **************************************************************************************************************************************************************************************
changed: [test3]

RUNNING HANDLER [Reload Apache] ******************************************************************************************************************************************************************************
changed: [test3]

RUNNING HANDLER [Restart Apache] *****************************************************************************************************************************************************************************
changed: [test3]

PLAY RECAP ***************************************************************************************************************************************************************************************************
test3                      : ok=22   changed=19   unreachable=0    failed=0    skipped=0    rescued=0    ignored=0


```
review completion
[fresh](http://13.228.168.1/2021/02/07/hello-world/)


### Option 2 - Terraform EC2 with wordpress AMI
simple provision of ec2 tiny instance using wordpress ami
```terraform
provider "aws" {
    region = "ap-southeast-1"  
}
resource "aws_instance" "wordpress"{
    ami = "ami-0b64e35366a2cbe73"
    instance_type = "t2.micro"
}
```
result
```
$ terraform apply

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_instance.wordpress will be created
  + resource "aws_instance" "wordpress" {
      + ami                          = "ami-0b64e35366a2cbe73"
      + arn                          = (known after apply)
      + associate_public_ip_address  = (known after apply)
      + availability_zone            = (known after apply)
      + cpu_core_count               = (known after apply)
      + cpu_threads_per_core         = (known after apply)
      + get_password_data            = false
      + host_id                      = (known after apply)
      + id                           = (known after apply)
      + instance_state               = (known after apply)
      + instance_type                = "t2.micro"
      + ipv6_address_count           = (known after apply)
      + ipv6_addresses               = (known after apply)
      + key_name                     = (known after apply)
      + outpost_arn                  = (known after apply)
      + password_data                = (known after apply)
      + placement_group              = (known after apply)
      + primary_network_interface_id = (known after apply)
      + private_dns                  = (known after apply)
      + private_ip                   = (known after apply)
      + public_dns                   = (known after apply)
      + public_ip                    = (known after apply)
      + secondary_private_ips        = (known after apply)
      + security_groups              = (known after apply)
      + source_dest_check            = true
      + subnet_id                    = (known after apply)
      + tenancy                      = (known after apply)
      + vpc_security_group_ids       = (known after apply)

      + ebs_block_device {
          + delete_on_termination = (known after apply)
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + snapshot_id           = (known after apply)
          + tags                  = (known after apply)
          + throughput            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }

      + enclave_options {
          + enabled = (known after apply)
        }

      + ephemeral_block_device {
          + device_name  = (known after apply)
          + no_device    = (known after apply)
          + virtual_name = (known after apply)
        }

      + metadata_options {
          + http_endpoint               = (known after apply)
          + http_put_response_hop_limit = (known after apply)
          + http_tokens                 = (known after apply)
        }

      + network_interface {
          + delete_on_termination = (known after apply)
          + device_index          = (known after apply)
          + network_interface_id  = (known after apply)
        }

      + root_block_device {
          + delete_on_termination = (known after apply)
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + tags                  = (known after apply)
          + throughput            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }
    }

Plan: 1 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

aws_instance.wordpress: Creating...
aws_instance.wordpress: Still creating... [10s elapsed]
aws_instance.wordpress: Creation complete after 15s [id=i-0e6bfe53495e86f20]

Apply complete! Resources: 1 added, 0 changed, 0 destroyed.
```
[review](http://13.229.48.235)


### Option 3 - Terraform ECS with wordpress image from dockerhub
build and push wordpress image to ECR.
run terraform script to create a ECS cluster with task container running wordpress
```terraform 
provider "aws" {
  region = "ap-southeast-1"
}

resource "aws_ecs_cluster" "cluster" {
  name               = "terraform-ecs-cluster"
  capacity_providers = ["FARGATE_SPOT", "FARGATE"]
  default_capacity_provider_strategy {
    capacity_provider = "FARGATE_SPOT"
  }
  setting {
    name  = "containerInsights"
    value = "disabled"
  }
}
module "ecs-fargate" {
  source                          = "umotif-public/ecs-fargate/aws"
  version                         = "~> 5.1.0"
  name_prefix                     = "ecs-fargate-example"
  container_name                  = "ecs-fargate-example2"
  vpc_id                          = "vpc-05a9e3389b85681f1"
  private_subnet_ids              = ["subnet-0362a6b27c739b179"]
  cluster_id                      = aws_ecs_cluster.cluster.id
  task_container_image            = "704361135933.dkr.ecr.ap-southeast-1.amazonaws.com/test:latest"
  desired_count                   = 1
  task_definition_cpu             = 256
  task_definition_memory          = 512
  task_container_port             = 80
  task_container_assign_public_ip = true
  lb_arn                          = "arn:aws:elasticloadbalancing:ap-southeast-1:704361135933:loadbalancer/app/test-alb/b839e2cb092f7b06"
  health_check = {
    port = "traffic-port"
    path = "/"
  }
  health_check_grace_period_seconds = 3000
  tags = {
    Environment = "test"
    Project     = "Test"
  }
 }

```
## Monitoring
Option 1 – ManageWP\
https://managewp.com/ \
My wordpress site: http://13.228.168.1/2021/02/07/hello-world/

 -connect website\
 -uptime monitoring\
 -adjust interval\

Option 2 - Add a Application Load Balancer \
